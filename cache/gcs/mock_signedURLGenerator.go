// Code generated by mockery v2.14.0. DO NOT EDIT.

package gcs

import (
	storage "cloud.google.com/go/storage"
	mock "github.com/stretchr/testify/mock"
)

// mockSignedURLGenerator is an autogenerated mock type for the signedURLGenerator type
type mockSignedURLGenerator struct {
	mock.Mock
}

// Execute provides a mock function with given fields: bucket, name, opts
func (_m *mockSignedURLGenerator) Execute(bucket string, name string, opts *storage.SignedURLOptions) (string, error) {
	ret := _m.Called(bucket, name, opts)

	var r0 string
	if rf, ok := ret.Get(0).(func(string, string, *storage.SignedURLOptions) string); ok {
		r0 = rf(bucket, name, opts)
	} else {
		r0 = ret.Get(0).(string)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, string, *storage.SignedURLOptions) error); ok {
		r1 = rf(bucket, name, opts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

type mockConstructorTestingTnewMockSignedURLGenerator interface {
	mock.TestingT
	Cleanup(func())
}

// newMockSignedURLGenerator creates a new instance of mockSignedURLGenerator. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func newMockSignedURLGenerator(t mockConstructorTestingTnewMockSignedURLGenerator) *mockSignedURLGenerator {
	mock := &mockSignedURLGenerator{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
