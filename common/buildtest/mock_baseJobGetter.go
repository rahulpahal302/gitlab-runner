// Code generated by mockery v2.14.0. DO NOT EDIT.

package buildtest

import (
	mock "github.com/stretchr/testify/mock"
	common "gitlab.com/gitlab-org/gitlab-runner/common"
)

// mockBaseJobGetter is an autogenerated mock type for the baseJobGetter type
type mockBaseJobGetter struct {
	mock.Mock
}

// Execute provides a mock function with given fields:
func (_m *mockBaseJobGetter) Execute() (common.JobResponse, error) {
	ret := _m.Called()

	var r0 common.JobResponse
	if rf, ok := ret.Get(0).(func() common.JobResponse); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(common.JobResponse)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func() error); ok {
		r1 = rf()
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

type mockConstructorTestingTnewMockBaseJobGetter interface {
	mock.TestingT
	Cleanup(func())
}

// newMockBaseJobGetter creates a new instance of mockBaseJobGetter. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func newMockBaseJobGetter(t mockConstructorTestingTnewMockBaseJobGetter) *mockBaseJobGetter {
	mock := &mockBaseJobGetter{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
